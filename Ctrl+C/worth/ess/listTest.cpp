#include<iostream>
using std::cout;
#include<list>
using std::list;
#include <functional>
using std::greater;

/*
 * Somewhere, I read that lists store elements
 * in memory locations that never change. I know
 * it was in a stackoverflow discussion and the
 * guy provided a quote from some documentation.
 * I don't see it say this in cplusplus.com so
 * this will test various scenarios.
 */


int main()
{
	list<int> l;
	int n = 7;
	list<int>::iterator i;
	//push 7 integers in reverse order to the list
	for(int i = n; i>0; i--)l.push_back(i);

	//display values and their memory locations
	for(i = l.begin(); i!= l.end(); i++)
		cout<<(*i)<<'\t'<<(&*i)<<'\n';
	
	cout<<'\n';
	//sort ascending
	l.sort(); //~Theta(nlog(n))

	/*
	 * list::sort is less efficient than stl Sort:
	 * O(nlog(n))
	 * on average: Omega(n)
	 *
	 * But list strictly keeps pointer addresses.
	*/
	
	//display values and their memory locations
	for(i = l.begin(); i!= l.end(); i++)
		cout<<(*i)<<'\t'<<(&*i)<<'\n';
	
	cout<<'\n';
	//sort descending
	l.sort(greater<int>());
	
	//display values and their memory locations
	for(i = l.begin(); i!= l.end(); i++)
		cout<<(*i)<<'\t'<<(&*i)<<'\n';

	cout<<'\n';
	//remove 5 from the list.
	l.remove(5);

	//display values and their memory locations
	for(i = l.begin(); i!= l.end(); i++)
		cout<<(*i)<<'\t'<<(&*i)<<'\n';
	
	cout<<'\n';
	//push a new 5 to the list
	l.push_back(12);
	
	//display values and their memory locations
	for(i = l.begin(); i!= l.end(); i++)
		cout<<(*i)<<'\t'<<(&*i)<<'\n';
	
	cout<<'\n';
}

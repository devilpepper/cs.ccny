#include<iostream>
using std::cout;
using std::endl;
int main()
{
	bool inSet;
	int i;
	for(int j=0; j<1024; j++)
	{
		i=j;
		inSet = (i%8 == 0 && (i/=8) > 0);
		while(inSet && ((i & 0xF) == 0)) i /= 16;
		i &= 0xF;
		inSet &= !(i == 2 || i == 6 || i == 8 || i == 10 || i == 14);
		if(inSet) cout<<j<<endl;
	}
	return 0;
}

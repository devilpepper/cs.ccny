#include<iostream>
using std::cout;
using std::endl;

class refReturn
{
	public:
		refReturn(int val){this->x = val;}
		int getX(){return this->x;}
		int& getXbyRef(){return this->x;}
		const int& getConstX(){return this->x;}
		int* getXpointer(){return &this->x;}
		const int* getXpConst(){return &this->x;}
	private:
		int x;
};

int main()
{
	refReturn x(7);
	cout<<"getX(): "<<x.getX()<<endl;
	int y = x.getXbyRef();
	cout<<"y = getXbyRef(): "<<y<<endl;
	y++;
	cout<<"y++; getX(): "<<x.getX()<<endl;
	cout<<"y: "<<y<<endl;
	int z = x.getConstX();
	cout<<"z = getConstX(): "<<z<<endl;
	z++;
	cout<<"z++; getX(): "<<x.getX()<<endl;
	cout<<"z: "<<z<<endl;
	int* w = x.getXpointer();
	cout<<"w = getXpointer(): "<<(*w)<<endl;
	(*w)++;
	cout<<"w++; getX(): "<<x.getX()<<endl;
	const int* v = x.getXpConst();
	cout<<"v = getXpConst(): "<<(*v)<<endl;
	cout<<"v++; getX(): "<<x.getX()<<endl;
	int& t = x.getXbyRef();
	cout<<"&t = getXbyRef(): "<<t<<endl;
	t++;
	cout<<"t++; getX(): "<<x.getX()<<endl;
	cout<<"t: "<<t<<endl;
	const int& u = x.getConstX();
	cout<<"&u = getConstX(): "<<u<<endl;
	cout<<"u++; getX(): "<<x.getX()<<endl;
	return 0;
}

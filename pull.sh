#!/bin/bash

find . -type d -name .git -exec sh -c "echo \"{}\" && cd \"{}\"/../ && git pull --no-edit" \;

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo contains copies of my professor's lecture notes and projects, along with anything I completed.

### How do I get set up? ###

* git clone https://SupaStuff@bitbucket.org/SupaStuff/cs.ccny.git
* while in class, pull.sh is in the root of this repo. ./pull.sh should get any updates from the professor.

### Contribution guidelines ###

* No contributing. This is here for me to review if I have to...
* In fact, none of my classmates should be here. At least until the class ends.
* filename_old.extension is the file that I got from the professor before I wrote in it. If there's no filename_old, I didn't need to modify it.
* For practice, filename_old has TODOs. filename has the TODOs done and they compile.

### Who do I talk to? ###

* Myself, I guess.
* If I'm still taking these classes and I need to review... Office hours.